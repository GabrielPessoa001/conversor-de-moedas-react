import * as React from "react";

import Accordion from '../Utilities/Accordion'

import './index.scss'

import logo from '../../images/logo.png'

export default class Sidebar extends React.Component {
	constructor (props) {
		super(props);

		this.state = {  }
	}

	render () {
		return (
			<div className="sidebar">
				<img className="logo" src={ logo } />

				<Accordion 
					title="Home"
					src="/"
				/>

				<Accordion 
					title="BitCoins"
					src="bitcoins"
				/>

				<Accordion 
					title="Moedas"
					src="coins"
				/>
			</div>
		);
	};
};