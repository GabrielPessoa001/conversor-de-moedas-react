import React, { Component } from 'react'

import './index.scss'

export default class BitCoin extends Component {
  constructor (props) {
    super(props)

    this.state = {
      number_bitcoin: 0,
      number_converted: 0,
      values_btc: 0,
      loading: true
    }

    this.setNumberBitCoin = this.setNumberBitCoin.bind(this)
    this.converter_value = this.converter_value.bind(this)
  }

  componentDidMount () {
		let URL_BTC = "https://www.mercadobitcoin.net/api/BTC/ticker/"

		fetch(URL_BTC)
    .then(res => res.json())
    .then(data => {
			this.setState({ values_btc: data, loading: false })
		})
    .catch(err => {
      throw err;
		});
	}

  setNumberBitCoin (event) {
    this.setState({ number_bitcoin: event.target.value })
  }

  converter_value () {
    let cotacao_bitcoin = this.state.values_btc.ticker 
    this.setState({ number_converted: this.state.number_bitcoin*cotacao_bitcoin.open })
  }

  render () {
    const { loading } = this.state

    console.log(this.state.values_btc.ticker)

    return (
      <div className="content_bitcoin">
        <input onChange={ this.setNumberBitCoin } className="input_number_bitcoin" placeholder="0" type="number" />

        <h1 className="header_number_converted">{ this.state.number_converted }</h1>

        <button onClick={ this.converter_value }>Converter</button>

        <div className="values_cotacao">
          {
            loading
            ?
            <div className="container_bitcoins nice_font">
              Loading
            </div>
            :
            <div className="container_bitcoins">
              {
                Object.keys(this.state.values_btc.ticker).map(value => ( <p className="nice_font">{ value }</p> ))
              }
            </div>
          }
        </div>
      </div>
    );
  };
};