import * as React from "react";

import Converter  from '../Converter'
import BitCoin    from '../BitCoin'
import Dashboard  from '../Dashboard'

import Sidebar    from '../Sidebar'
import Navbar     from '../Navbar'

import {  BrowserRouter,
          Switch,
          Route } from 'react-router-dom'

import './index.scss'

export default class App extends React.Component {
  constructor (props) {
    super(props);

    this.state = { }
  }

  render () {
    return (
      <BrowserRouter>
        <div className="container">
          <Sidebar />

          <div className="pusher">
            <Navbar />

            <Route path="/" exact={ true } component={ Dashboard } />
            <Route path="/coins" exact={ true } component={ Converter } />
            <Route path="/bitcoins" exact={ true } component={ BitCoin } />
          </div>
        </div>
      </BrowserRouter>
    );
  };
};
