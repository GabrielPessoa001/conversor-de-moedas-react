import React, { Component } from 'react'

import './index.scss'

export default class Accordion extends Component {
  constructor (props) {
    super(props);

    this.state = {  }
  }

  render () {
    return (
      <div>
        <div className="accordion">
          <a href={ this.props.src } className="accordion_title">{ this.props.title }</a>

          {
            this.props.content
            ?
            <p className="accordion_body">
              { this.props.content }
            </p>
            :
            <div></div>
          }
        </div>
      </div>
    );
  };
};