import * as React from "react";

import './index.scss'

export default class Converter extends React.Component {
	constructor (props) {
		super(props);

		this.state = { 
			conversion_number: "",
			converted_number: "0",
			from_converted: "USD",
			to_converted: "BRL",
			loading: true,
			coins: []
		}

		this.setConversionNumber = this.setConversionNumber.bind(this)
		this.converted = this.converted.bind(this)

		this.setFromConverted = this.setFromConverted.bind(this)
		this.setToConverted = this.setToConverted.bind(this)
	}

	componentDidMount() {
		let URL_COINS = "https://economia.awesomeapi.com.br/json/all"

		fetch(URL_COINS)
    .then(res => res.json())
    .then(data => {
			this.setState({ coins: data, loading: false })
		})
    .catch(err => {
      throw err;
		});
	}

	setConversionNumber (event) {
		this.setState({ conversion_number: event.target.value })
	}

	setConvertedNumber (event) {
		this.setState({ converted_number: event.target.value })
	}

	setFromConverted (event) {
		this.setState({ from_converted: event.target.value.toUpperCase() })
	}

	setToConverted (event) {
		this.setState({ to_converted: event.target.value.toUpperCase() })
	}

	converted () {
		let URL_COTACAO = `http://economia.awesomeapi.com.br/json/${ this.state.from_converted }-${ this.state.to_converted }/1`

		console.log(URL_COTACAO)

		fetch(URL_COTACAO)
    .then(res => res.json())
    .then(data => {
			if (data[0]) {
				const cotacao = data[0]["high"]
				this.setState({ converted_number: this.state.conversion_number*cotacao })
			} else {
				this.setState({ converted_number: "Código não encontrado" })
			}
		})
    .catch(err => {
      throw err;
    });
	}

	render () {
		const { loading, coins } = this.state

		return (
			<div className="content_converter">
				<input className="conversion_number" type="number" onChange={ this.setConversionNumber } placeholder="0" /><br /><br />

				<div>
					<input className="input_text_converted margin_right" onChange={ this.setFromConverted } placeholder="BRL" />
					para
					<input className="input_text_converted margin_left" onChange={ this.setToConverted } placeholder="USD" />
				</div>

				<h1 className="converted_number">{ this.state.converted_number }</h1>

				<button className="button_converter" onClick={ this.converted }>Converter</button>
			
				{ 
					loading
					?
					<div className="container_coins nice_font">loading</div>
					:
					<div className="container_coins">
						{ Object.keys(this.state.coins).map(coin => ( <p className="nice_font">{ this.state.coins[coin].name }: { this.state.coins[coin].code }</p> )) }
					</div> 
				}
			</div>
		);
	};
};
